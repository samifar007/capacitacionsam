﻿using System;
using Pacagroup.Ecommerce.Transversal.Common;
using Microsoft.Extensions.Logging;

namespace Pacagroup.Ecommerce.Transversal.Logging
{
    public class LoggerAdapter<T> : IAppLogger<T>
    {
        private readonly ILogger<T> _logger;

        public LoggerAdapter(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<T>();
        }
        public void LogError(string menssage, params object[] args)
        {
            _logger.LogError(menssage, args);
        }

        public void LogInformation(string menssage, params object[] args)
        {
            _logger.LogInformation(menssage, args);
        }

        public void LogWarning(string menssage, params object[] args)
        {
            _logger.LogWarning(menssage, args);
        }
    }
}
