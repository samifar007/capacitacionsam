﻿namespace Pacagroup.Ecommerce.Transversal.Common
{
    public interface IAppLogger<T>
    {
        void LogInformation(string menssage, params object[] args);
        void LogWarning(string menssage, params object[] args);
        void LogError(string menssage, params object[] args);
    }
}
