﻿using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository
{
    public class ProductoraRepository : IProductoraRepository
    {
        private readonly ApplicationDbContext _bd;

        public ProductoraRepository(ApplicationDbContext bd)
        {
            _bd = bd;
        }
        public bool ActualizarProductora(Productora productora)
        {
            _bd.Productora.Update(productora);
            return Guardar();
        }

        public bool BorrarProductora(Productora productora)
        {
            _bd.Productora.Remove(productora);
            return Guardar();
        }

        public bool CrearProductora(Productora productora)
        {
            _bd.Productora.Add(productora);
            return Guardar();
        }

        public bool ExisteProductora(int id)
        {
            return _bd.Productora.Any(c => c.Id == id);
        }

        public bool ExisteProductora(string nombre)
        {
            bool valor = _bd.Productora.Any(c => c.Nombre.ToLower().Trim() == nombre.ToLower().Trim());
            return valor;
        }

        public Productora GetProductora(int ProductoraId)
        {
            return _bd.Productora.FirstOrDefault(c => c.Id == ProductoraId);
        }

        public ICollection<Productora> GetProductoras()
        {
            return _bd.Productora.OrderBy(c => c.Nombre).ToList();
        }

        public bool Guardar()
        {
            return _bd.SaveChanges() >= 0 ? true : false;
        }
    }
}
