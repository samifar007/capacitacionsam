﻿using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository
{
    public class DirectorRepository : IDirectorRepository
    {
        private readonly ApplicationDbContext _bd;

        public DirectorRepository(ApplicationDbContext bd)
        {
            _bd = bd;
        }
        public bool ActualizarDirector(Director director)
        {
            _bd.Director.Update(director);
            return Guardar();
        }

        public bool BorrarDirector(Director director)
        {
            _bd.Director.Remove(director);
            return Guardar();
        }

        public bool CrearDirector(Director director)
        {
            _bd.Director.Add(director);
            return Guardar();
        }

        public bool ExisteDirector(string nombre)
        {
            bool valor = _bd.Director.Any(c => c.Nombre.ToLower().Trim() == nombre.ToLower().Trim());
            return valor;
        }

        public bool ExisteDirector(int id)
        {
            return _bd.Director.Any(c => c.Id == id);
        }

        public Director GetDirector(int DirectorId)
        {
            return _bd.Director.FirstOrDefault(c => c.Id == DirectorId);
        }

        public ICollection<Director> GetDirectores()
        {
            return _bd.Director.OrderBy(c => c.Nombre).ToList();
        }

        public bool Guardar()
        {
            return _bd.SaveChanges() >= 0 ? true : false;
        }
    }
}
