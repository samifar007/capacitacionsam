﻿using ApiPeliculas.Data;
using ApiPeliculas.Models;
using ApiPeliculas.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository
{
    public class CalificacionRepository : ICalificacionRepository
    {
        private readonly ApplicationDbContext _bd;

        public CalificacionRepository(ApplicationDbContext bd)
        {
            _bd = bd;

        }
        public bool ActualizarPuntaje(Calificacion calificacion)
        {
             _bd.Calificacion.Update(calificacion);
            return Guardar();
        }

        public bool DarPuntaje(Calificacion calificacion)
        {
            _bd.Calificacion.Add(calificacion);
            return Guardar();
        }

        public bool ExisteCalificacion(int IdPuntaje)
        {
            return _bd.Calificacion.Any(c => c.Id == IdPuntaje);
        }
        public bool ExisteCalificacionPorUsuarioYPelicula(int IdUsuario, int IdPelicula)
        {
            return _bd.Calificacion.Any(c => c.UsuarioId == IdUsuario && c.PeliculaId == IdPelicula);
        }
        public bool ExisteCalificacionPorUsuarioYPelicula(int IdUsuario, int IdPelicula, int IdCalificacion)
        {
            return _bd.Calificacion.Any(c => c.UsuarioId == IdUsuario && c.PeliculaId == IdPelicula && c.Id == IdCalificacion);
        }

        public Calificacion GetPuntajePorId(int IdPuntaje)
        {
            return _bd.Calificacion.Include(ca => ca.Pelicula).Include(us => us.Usuario).FirstOrDefault(c => c.Id == IdPuntaje);
        }

        public ICollection<Calificacion> GetPuntajes()
        {
            return _bd.Calificacion.Include(ca => ca.Pelicula).Include(us => us.Usuario).ToList();
        }

        public ICollection<Calificacion> GetPuntajesPorPelicula(int IdPelicula)
        {
            return _bd.Calificacion.Include(ca => ca.Pelicula).Include(us=> us.Usuario).Where(c => c.PeliculaId == IdPelicula).ToList();
        }

        public ICollection<Calificacion> GetPuntajesPorUsuario(int IdUsuario)
        {
            return _bd.Calificacion.Include(ca => ca.Pelicula).Include(us => us.Usuario).Where(c => c.UsuarioId == IdUsuario).ToList();
        }

        public bool Guardar()
        {
            return _bd.SaveChanges() >= 0 ? true : false;
        }
    }
}
