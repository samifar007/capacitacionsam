﻿using ApiPeliculas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository.IRepository
{
    public interface IDirectorRepository
    {
        ICollection<Director> GetDirectores();
        Director GetDirector(int DirectorId);
        bool ExisteDirector(string nombre);
        bool ExisteDirector(int id);
        bool CrearDirector(Director director);
        bool ActualizarDirector(Director director);
        bool BorrarDirector(Director director);
        bool Guardar();
    }
}
