﻿using ApiPeliculas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository.IRepository
{
    public interface IProductoraRepository
    {
        ICollection<Productora> GetProductoras();
        Productora GetProductora(int ProductoraId);
        bool ExisteProductora(string nombre);
        bool ExisteProductora(int id);
        bool CrearProductora(Productora productora);
        bool ActualizarProductora(Productora productora);
        bool BorrarProductora(Productora productora);
        bool Guardar();
    }
}
