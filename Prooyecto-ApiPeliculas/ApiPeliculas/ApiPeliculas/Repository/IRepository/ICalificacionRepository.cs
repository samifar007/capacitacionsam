﻿using ApiPeliculas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Repository.IRepository
{
    public interface ICalificacionRepository
    {
        ICollection<Calificacion> GetPuntajes();
        ICollection<Calificacion> GetPuntajesPorPelicula(int IdPelicula);
        ICollection<Calificacion> GetPuntajesPorUsuario(int IdUsuario);
        Calificacion GetPuntajePorId(int IdPuntaje);
        bool ExisteCalificacion(int IdPuntaje);
        bool ExisteCalificacionPorUsuarioYPelicula(int IdUsuario, int IdPelicula);
        bool ExisteCalificacionPorUsuarioYPelicula(int IdUsuario, int IdPelicula, int IdCalificacion);
        bool DarPuntaje(Calificacion calificacion);
        bool ActualizarPuntaje(Calificacion calificacion);
        bool Guardar();
    }
}
