﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models.Dtos
{
    public class CalificacionGetDto
    {
        public int Id { get; set; }
        public Usuario Usuario { get; set; }
        public Pelicula Pelicula { get; set; }
        public int Puntaje { get; set; }
    }
}
