﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models.Dtos
{
    public class CalificacionUpdateDto
    {
        public int UsuarioId { get; set; }
        public int PeliculaId { get; set; }
        public int Puntaje { get; set; }
    }
}
