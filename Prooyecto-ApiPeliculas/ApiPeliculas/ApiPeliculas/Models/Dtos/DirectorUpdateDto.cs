﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models.Dtos
{
    public class DirectorUpdateDto
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }
}
