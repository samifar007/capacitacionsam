﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class Categoria
    {
        public int Id { get ;set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
