﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Models
{
    public class Calificacion
    {
        [Key]
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        
        [ForeignKey("UsuarioId")]
        public Usuario Usuario { get; set; }
        public int PeliculaId { get; set; }
        
        [ForeignKey("PeliculaId")]
        public Pelicula Pelicula { get; set; }
        public int Puntaje { get; set; }
    }
}
