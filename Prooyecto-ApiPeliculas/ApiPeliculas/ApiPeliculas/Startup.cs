using ApiPeliculas.Data;
using ApiPeliculas.helpers;
using ApiPeliculas.PeliculasMapper;
using ApiPeliculas.Repository;
using ApiPeliculas.Repository.IRepository;
using ApiUsuarios.Repository.IRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ApiPeliculas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(Options => Options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<ICategoriaRepository, CategoriaRepository>();
            services.AddScoped<IPeliculaRepository, PeliculaRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<IProductoraRepository, ProductoraRepository>();
            services.AddScoped<IDirectorRepository, DirectorRepository>();
            services.AddScoped<ICalificacionRepository, CalificacionRepository>();

            /*Agregar Dependencia de Token*/
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => 
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddAutoMapper(typeof(PeliculasMappers));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
            c.SwaggerDoc("ApiPeliculasCategorias", new OpenApiInfo
            {
                Title = "Api Catergorias Peliculas",
                Version = "v1",
                Description = "Backend Peliculas",
                Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                {
                    Email = "admin@render2web.com",
                    Name = "render2web",
                    Url = new Uri("https://render2web.com")
                },
                License = new Microsoft.OpenApi.Models.OpenApiLicense()
                {
                    Name = "MIT License",
                    Url = new Uri("https://en.wikipedia.org/wiki/MIT_License")
                }
            });

            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "ApiPeliculas",
                Version = "v1",
                Description = "Backend Peliculas"
            });

            c.SwaggerDoc("ApiPeliculasUsuarios", new OpenApiInfo
            {
                Title = "Api Usuarios Peliculas",
                Version = "v1",
                Description = "Backend Peliculas"
            });

            c.SwaggerDoc("ApiPeliculasCalificacion", new OpenApiInfo
            {
                Title = "Api Calificacion Peliculas",
                Version = "v1",
                Description = "Backend Peliculas"
            });

            c.SwaggerDoc("ApiPeliculasDirectores", new OpenApiInfo
            {
                Title = "Api Directores Peliculas",
                Version = "v1",
                Description = "Backend Peliculas"
            });

            c.SwaggerDoc("ApiPeliculasProductoras", new OpenApiInfo
            {
                Title = "Api Productoras Peliculas",
                Version = "v1",
                Description = "Backend Peliculas"
            });

                var archivoXmlComentarios = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var rutaApiComentarios = Path.Combine(AppContext.BaseDirectory, archivoXmlComentarios);
            c.IncludeXmlComments(rutaApiComentarios);

            /*Definir esquema de Seguridad*/

            c.AddSecurityDefinition("Bearer",
                new OpenApiSecurityScheme
                {
                    Description = "Autenticacion JWT (Bearer)",
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }

                        }, new List<string>()
                     }
                });
            });

            /*Soporte para Cors*/

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "ApiPeliculas v1");
                    c.SwaggerEndpoint("/swagger/ApiPeliculasCalificacion/swagger.json", "Api Calificacion Peliculas v1");
                    c.SwaggerEndpoint("/swagger/ApiPeliculasCategorias/swagger.json", "Api Categoria Peliculas v1");
                    c.SwaggerEndpoint("/swagger/ApiPeliculasDirectores/swagger.json", "Api Directores Peliculas v1");
                    c.SwaggerEndpoint("/swagger/ApiPeliculasProductoras/swagger.json", "Api Productoras Peliculas v1");
                    c.SwaggerEndpoint("/swagger/ApiPeliculasUsuarios/swagger.json", "Api Usuarios Peliculas v1");
                });
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                builder.Run(async context =>{
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    var error = context.Features.Get<IExceptionHandlerFeature>();

                    if(error != null)
                    {
                        context.Response.AddApplicationError(error.Error.Message);
                        await context.Response.WriteAsync(error.Error.Message);
                    }

                });

                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            /*Para autenticacion y autorizacion*/

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            /*Soporte para Cors*/

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        }
    }
}
