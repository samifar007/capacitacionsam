﻿using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Controllers
{
    [Route("api/calificaciones")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "ApiPeliculasCalificacion")]

    public class CalificacionesController : ControllerBase
    {
        private readonly ICalificacionRepository _ctCali;
        private readonly IMapper _mapper;
        public CalificacionesController(ICalificacionRepository ctCali, IMapper mapper)
        {
            _ctCali = ctCali;
            _mapper = mapper;
        }

        [HttpGet("GetPuntajes")]
        public IActionResult GetPuntajes()
        {
            var listaCalificaciones = _ctCali.GetPuntajes();

            var listaCalificacionesDto = new List<CalificacionGetDto>();

            foreach (var lista in listaCalificaciones)
            {
                listaCalificacionesDto.Add(_mapper.Map<CalificacionGetDto>(lista));
            }
            return Ok(listaCalificacionesDto);
        }


        [HttpGet("GetPuntajesPorPelicula/{IdPelicula:int}")]
        public IActionResult GetPuntajesPorPelicula(int IdPelicula)
        {
            var listaCalificaciones = _ctCali.GetPuntajesPorPelicula(IdPelicula);

            var listaCalificacionesDto = new List<CalificacionGetDto>();

            foreach (var lista in listaCalificaciones)
            {
                listaCalificacionesDto.Add(_mapper.Map<CalificacionGetDto>(lista));
            }
            return Ok(listaCalificacionesDto);
        }

        [HttpGet("GetPuntajesPorUsuario/{IdUsuario:int}")]
        public IActionResult GetPuntajesPorUsuario(int IdUsuario)
        {
            var listaCalificaciones = _ctCali.GetPuntajesPorUsuario(IdUsuario);

            var listaCalificacionesDto = new List<CalificacionGetDto>();

            foreach (var lista in listaCalificaciones)
            {
                listaCalificacionesDto.Add(_mapper.Map<CalificacionGetDto>(lista));
            }
            return Ok(listaCalificacionesDto);
        }

        [HttpPost("DarPuntaje")]
        public IActionResult DarPuntaje([FromBody] CalificacionDto calificacionDto)
        {
            if (calificacionDto == null)
            {
                return BadRequest(ModelState);
            }
            

            if (_ctCali.ExisteCalificacionPorUsuarioYPelicula(calificacionDto.UsuarioId, calificacionDto.PeliculaId))
            {
                ModelState.AddModelError("", "La calificacion ya existe");
                return StatusCode(404, ModelState);
            }

            var calificacion = _mapper.Map<Calificacion>(calificacionDto);
            if (!_ctCali.DarPuntaje(calificacion))
            {
                ModelState.AddModelError("", $"Algo salio mal guardando el registro");
                return StatusCode(500, ModelState);

            }
            return CreatedAtRoute("GetCategoria", new { categoriaId = calificacion.Id }, calificacion);
        }

        [HttpPatch("ActualizarPuntaje")]
        public IActionResult ActualizarPuntaje(int calificacionId, [FromBody] CalificacionUpdateDto calificacionUpdateDto)
        {
            if (calificacionUpdateDto == null || !_ctCali.ExisteCalificacion(calificacionId))
            {
                return BadRequest(ModelState);
            }

            if (!_ctCali.ExisteCalificacionPorUsuarioYPelicula(calificacionUpdateDto.UsuarioId, calificacionUpdateDto.PeliculaId, calificacionId))
            {
                ModelState.AddModelError("", "Datos Invalidos");
                return StatusCode(404, ModelState);
            }


            var calificacion = _mapper.Map<Calificacion>(calificacionUpdateDto);
            calificacion.Id = calificacionId;

            if (!_ctCali.ActualizarPuntaje(calificacion))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el registro");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}
