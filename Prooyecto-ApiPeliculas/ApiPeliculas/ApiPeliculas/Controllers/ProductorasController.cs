﻿using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Controllers
{
    [Route("api/productoras")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "ApiPeliculasProductoras")]

    public class ProductorasController : ControllerBase
    {
        private readonly IProductoraRepository _ctPro;
        private readonly IMapper _mapper;
        public ProductorasController(IProductoraRepository ctRepo, IMapper mapper)
        {
            _ctPro = ctRepo;
            _mapper = mapper;
        }

        [HttpGet("GetProductoras")]
        public IActionResult GetProductoras()
        {
            var listaProductoras = _ctPro.GetProductoras();
            var listaProductorasDto = new List<ProductoraDto>();

            foreach(var lista in listaProductoras)
            {
                listaProductorasDto.Add(_mapper.Map<ProductoraDto>(lista));
            }
            return Ok(listaProductorasDto);
        }

        [HttpGet("GetProductora/{productoraId:int}")]
        public IActionResult GetProductora(int productoraId)
        {
            var itemProductora = _ctPro.GetProductora(productoraId);
            if (itemProductora == null)
            {
                return NotFound();
            }

            var itemProductoraDto = _mapper.Map<ProductoraDto>(itemProductora);
            return Ok(itemProductoraDto);
        }

        [HttpPost("CrearProductora")]
        public IActionResult CrearProductora([FromBody] ProductoraDto productoraDto)
        {
            if (productoraDto == null)
            {
                return BadRequest(ModelState);
            }

            if (_ctPro.ExisteProductora(productoraDto.Nombre))
            {
                ModelState.AddModelError("", "La productora ya existe");
                return StatusCode(404, ModelState);
            }

            var productora = _mapper.Map<Productora>(productoraDto);
            if (!_ctPro.CrearProductora(productora))
            {
                ModelState.AddModelError("", $"Algo salio mal guardando el registro{productora.Nombre}");
                return StatusCode(500, ModelState);

            }
            return CreatedAtRoute("GetProductora", new { productoraId = productora.Id }, productora);
        }

        [HttpPatch("ActualizarProductora/{productoraId:int}")]
        public IActionResult ActualizarProductora(int productoraId, [FromBody] ProductoraUpdateDto productoraUpdateDto)
        {
            if (productoraUpdateDto == null || !_ctPro.ExisteProductora(productoraId))
            {
                return BadRequest(ModelState);
            }
            var productora = _mapper.Map<Productora>(productoraUpdateDto);
            productora.Id = productoraId;

            if (!_ctPro.ActualizarProductora(productora))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el registro{productora.Nombre}");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }

        [HttpDelete("BorrarProductora/{productoraId:int}")]
        public IActionResult BorrarProductora(int productoraId)
        {

            if (!_ctPro.ExisteProductora(productoraId))
            {
                return NotFound();
            }

            var productora = _ctPro.GetProductora(productoraId);

            if (!_ctPro.BorrarProductora(productora))
            {
                ModelState.AddModelError("", $"Algo salio mal borrando el registro{productora.Nombre}");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}
