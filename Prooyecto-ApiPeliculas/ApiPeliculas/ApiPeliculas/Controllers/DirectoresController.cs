﻿using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;
using ApiPeliculas.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPeliculas.Controllers
{
    [Route("api/directores")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "ApiPeliculasDirectores")]

    public class DirectoresController : ControllerBase
    {
        private readonly IDirectorRepository _ctDirector;
        private readonly IMapper _mapper;
        public DirectoresController(IDirectorRepository ctDirector, IMapper mapper)
        {
            _ctDirector = ctDirector;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetDirectores()
        {
            var listaDirectores = _ctDirector.GetDirectores();

            var listaDirectoresDto = new List<DirectorDto>();

            foreach (var lista in listaDirectores)
            {
                listaDirectoresDto.Add(_mapper.Map<DirectorDto>(lista));
            }
            return Ok(listaDirectoresDto);
        }

        [HttpGet("GetDirector/{directorId:int}")]
        public IActionResult GetDirector(int directorId)
        {
            var itemDirector = _ctDirector.GetDirector(directorId);
            if (itemDirector == null)
            {
                return NotFound();
            }
            var itemDirectorDto = _mapper.Map<DirectorDto>(itemDirector);
            return Ok(itemDirectorDto);
        }

        [HttpPost("CrearDirector")]
        public IActionResult CrearDirector([FromBody] DirectorUpdateDto directorUpdateDto)
        {
            if (directorUpdateDto == null)
            {
                return BadRequest(ModelState);
            }

            if (_ctDirector.ExisteDirector(directorUpdateDto.Nombre))
            {
                ModelState.AddModelError("", "El director ya existe");
                return StatusCode(404, ModelState);
            }

            var director = _mapper.Map<Director>(directorUpdateDto);
            if (!_ctDirector.CrearDirector(director))
            {
                ModelState.AddModelError("", $"Algo salio mal guardando el registro{director.Nombre}");
                return StatusCode(500, ModelState);

            }
            var dto = _mapper.Map<DirectorDto>(director);
            return Ok(dto);
        }
       
        [HttpPatch("ActualizarDirector/{directorId:int}")]
        public IActionResult ActualizarDirector(int directorId, [FromBody] DirectorUpdateDto directorUpdateDto)
        {
            if (directorUpdateDto == null || !_ctDirector.ExisteDirector(directorId))
            {
                return BadRequest(ModelState);
            }
            var director = _mapper.Map<Director>(directorUpdateDto);
            director.Id = directorId;

            if (!_ctDirector.ActualizarDirector(director))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el registro{director.Nombre}");
                return StatusCode(500, ModelState);
            }
            var dto = _mapper.Map<DirectorDto>(director);
            return Ok(dto);
        }

        [HttpDelete("BorrarDirector/{directorId:int}")]
        public IActionResult BorrarDirector(int directorId)
        {

            if (!_ctDirector.ExisteDirector(directorId))
            {
                return NotFound();
            }

            var director = _ctDirector.GetDirector(directorId);

            if (!_ctDirector.BorrarDirector(director))
            {
                ModelState.AddModelError("", $"Algo salio mal borrando el registro{director.Nombre}");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}
