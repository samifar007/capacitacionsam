﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiPeliculas.Models;
using ApiPeliculas.Models.Dtos;


namespace ApiPeliculas.PeliculasMapper
{
    public class PeliculasMappers: Profile
    {
        public PeliculasMappers()
        {
            CreateMap<Categoria,CategoriaDto>().ReverseMap();
            CreateMap<Categoria, CategoriaGetDto>().ReverseMap();
            CreateMap<Pelicula, PeliculaDto>()
                .ForMember(d => d.ProducidoPor, o => o.MapFrom(c => c.Productora.Nombre))
                .ReverseMap();
            CreateMap<Pelicula, PeliculaCreateDto>().ReverseMap();
            CreateMap<Pelicula, PeliculaUpdateDto>().ReverseMap();
            CreateMap<Usuario, UsuarioDto>().ReverseMap();
            CreateMap<Usuario, UsuarioAuthDto>().ReverseMap();
            CreateMap<Usuario, UsuarioAuthLoginDto>().ReverseMap();
            CreateMap<Productora, ProductoraDto>().ReverseMap();
            CreateMap<Productora, ProductoraUpdateDto>().ReverseMap();
            CreateMap<Director, DirectorDto>().ReverseMap();
            CreateMap<Director, DirectorUpdateDto>().ReverseMap();
            CreateMap<Calificacion, CalificacionDto>().ReverseMap();
            CreateMap<Calificacion, CalificacionUpdateDto>().ReverseMap();
            CreateMap<Calificacion, CalificacionGetDto>().ReverseMap();

        }
    }
}
